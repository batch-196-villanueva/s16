// alert("hi");

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Addition operator: " + sum);
// result: 9228

let difference = y - x;
console.log("Subtraction operator: " + difference);

let product = x * y;
console.log("Multiplication operator: " + product);

let quotient = x / y;
console.log("Division operator: " + quotient);

let remainder = y % x;
console.log("Modulo operator: " + remainder);
// result: 846

// Assignment Operator
	// Basic Assignment Operator (=)

let assignmentNumber = 8;

	// Addition Assignment Operator (+=)

// assignmentNumber = assignmentNumber + 2
// assignmentNumber = 8 + 2

assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);
// result: 10

assignmentNumber += 2;
console.log("Addition assignment: " + assignmentNumber);
// result: 12

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);
//result: BostonCeltics
//string1 = string1 + string2

assignmentNumber -= 2;
console.log("Subtraction assignment: " + assignmentNumber);
// result: 10

assignmentNumber *= 2;
console.log("Multiplication assignment: " + assignmentNumber);
// result: 20

assignmentNumber /= 2;
console.log("Division assignment: " + assignmentNumber);
// result: 10

// Multiple Operators and Parentheses
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("MDAS: " + mdas);
// result: 0.6
/*
- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("PEMDAS: " + pemdas);
// result: 0.199 or 0.2
/*
- By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations
	1. 4 / 5 = 0.8
	2. 2 - 3 = -1
	3. -1 * 0.8 = -0.8
	4. 1 + -0.8 = 0.2

*/

// Increment and Decrement
 // Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to 
let z = 1;

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment" 
let increment = ++z;
console.log("Pre-increment: " + increment);
// result: 2
// The value of "z" was also increased even though we didn't implicitly specify any value reassignment
console.log("Value of z: " + z);
// result: 2

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one 
increment = z++;
// The value of "z" is at 2 before it was incremented 
console.log("Post-increment: " + increment);
// result: 2
/* The value of "z" was increased again reassigning the value to 3 */
console.log("Value of z: " + z);
// result: 3

// The value of "z" is decreased by a value of one before returning the value and storing it in the variable "increment" 
let decrement = --z;
// The value of "z" is at 3 before it was decremented 
console.log("Pre-decrement: " + decrement);
// result: 2
 // The value of "z" was decreased reassigning the value to 2 
console.log("Value of z: " + z);
// result: 2

 // The value of "z" is returned and stored in the variable "increment" then the value of "z" is decreased by one 
decrement =  z--;
// The value of "z" is at 2 before it was decremented 
console.log("Post-decrement: " + decrement);
// result: 2
// The value of "z" was decreased reassigning the value to 1 
console.log("Value of z: " + z);
// result: 1

// Type Coercion
/*
  - Type coercion is the automatic or implicit conversion of values from one data type to another
  - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
  - Values are automatically converted from one data type to another in order to resolve operations
 */

let numA = '10';
let numB = 12;

/* 
    - Adding/concatenating a string and a number will result is a string
    - This can be proven in the console by looking at the color of the text displayed
    - Black text means that the output returned is a string data type
*/
let coercion = numA + numB;
console.log(coercion); // result: 1012
console.log(typeof coercion); // result: string

/* 
    - The result is a number
    - This can be proven in the console by looking at the color of the text displayed
    - Blue text means that the output returned is a number data type
*/
let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion); // result: 30
console.log(typeof nonCoercion); // result: number

/* 
  - The result is a number
  - The boolean "true" is also associated with the value of 1
*/
let numE = true + 1;
console.log(numE); // result: 2

/* 
    - The result is a number
    - The boolean "false" is also associated with the value of 0
*/
let numF = false + 1;
console.log(numF); // result: 1

// Equality Operator (==)
/* 
   - Checks whether the operands are equal/have the same content
   - Attempts to CONVERT AND COMPARE operands of different data types
   - Returns a boolean value
*/

console.log(1 == 1); // result: true
console.log(1 == 2); // result: false
console.log(1 == '1'); // result: true
console.log(false == 0); // result: true
console.log('johnny' == 'johnny') // result: true
console.log('Johnny' == 'johnny') // result: false

// Inequality Operator (!=)
/* 
    - Checks whether the operands are not equal/have different content
    - Attempts to CONVERT AND COMPARE operands of different data types
*/

console.log(1 != 1) // result: false
console.log(1 != 2) // result: true
console.log(1 != '1') // result: false
console.log(0 != false) // result: false
console.log('johnny' != 'johnny') // result: false
console.log('Johnny' != 'johnny') // result: true

// Strict Equality Operator (===)
/* 
   - Checks whether the operands are equal/have the same content
   - Also COMPARES the data types of 2 values
   - JavaScript is a loosely typed language meaning that values of different data types can be stored in variables
   - In combination with type coercion, this sometimes creates problems within our code (e.g. Java, Typescript)
   - Strict equality operators are better to use in most cases to ensure that data types provided are correct
*/

console.log(1 === 1); // result: true
console.log(1 === '1'); // result: false
console.log('johnny' === 'johnny'); // result: true

let johnny = 'johnny';
console.log('johnny' === johnny); // result: true
console.log(false === 0); // result: false

// Strict Inequality Operator (!==)
/* 
  - Checks whether the operands are not equal/have the same content
  - Also COMPARES the data types of 2 values
*/
console.log(1 !== 1); // result: false
console.log(1 !== 2); // result: true
console.log(1 !== '1'); // result: true
console.log('johnny' !== johnny) // result: false

// Relational Operator
	//Some comparison operators check whether one value is greater or less than to the other value.
	//Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
let a = 50;
let b = 65;

// GT or Greater Than (>)
let isGreaterThan = a > b;
console.log(isGreaterThan); //result: false

// LT or Less Than (<)
let isLessThan = a < b;
console.log(isLessThan); //result: true

// GTE or Greater Than or Equal (>=)
let isGTE = a >= b;
console.log(isGTE); //result: false

// LTE or Less Than or Equal (<=)
let isLTE = a <= b;
console.log(isLTE); //result: true

//forced coercion to change the string to a number.
let numStr = '30';
console.log(a > numStr); // result: true

let str = 'twenty';
console.log(b >= str); // result: false


// Logical Operators
	// AND Operator (&&)
	// Returns true if all operands are true 

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

let authorization1 = isAdmin && isRegistered;
console.log(authorization1); // result: false

let authorization2 = isLegalAge && isRegistered;
console.log(authorization2); // result: true

let authorization3 = isAdmin && isLegalAge;
console.log(authorization3); //result: false

let random = isAdmin && false;
console.log(random); // result: false

let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
console.log(authorization4); //result: false

let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
console.log(authorization5) // result: true

let userName = 'gamer2022';
let userName2 = 'theTinker';
let userAge= 15;
let userAge2 = 26;

let registration1 = userName.length > 8 && userAge >= requiredAge;
console.log(registration1); // result: false

let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); // result: true

	// OR Operator (||- Double Pipe)
	// Returns true if one of the operands are true 
let userLevel = 100;
let userLevel2 = 65;

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge
console.log(guildRequirement); // result: false

let guildRequirement2 = isRegistered || userLevel2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement2); // result: true

let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement3); // result: true

let guildAdmin = !isAdmin || userLevel2 >= requiredLevel;
console.log(guildAdmin); // result: true

// Not Operator (!)
// Returns the opposite value 
console.log(!isRegistered); // result: false

let opposite1 = !isAdmin;
console.log(opposite1); // result: true